from gtts import gTTS
import os

print('\33[4m CHOOSE LANGUAGE \033[42m(ex ru)\033[0m 🏳\033[94m')
language = str(input())
print('\033[4m Please write what you want to say 🤠')
myText = str(input())

try:
    myobj = gTTS(text=myText, lang=language, slow=False)
    myobj.save("test.mp3")
    if myText == "reboot my computer":
        os.system("reboot")
    else:
        os.system("play test.mp3")
except:
    print('\33[91m We have not this language ('+language+')')